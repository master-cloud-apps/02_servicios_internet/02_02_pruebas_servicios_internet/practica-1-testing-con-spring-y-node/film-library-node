const app = require('../../../src/app.js')
const request = require('supertest')
const { getTestFilm } = require('./../../index.js')
const { deleteInDynameFake, createFilmInDynamo, setDocumentClientAWS } = require('./../../db/index.js')
const dynamoFake = require('../../db/dynamoFake.js')

jest.mock('aws-sdk')

describe('GET /api/films', () => {
  beforeEach(() => setDocumentClientAWS(dynamoFake))
  test('Given one film put in Dynamo, when get films should return one element', () => {
    return createFilmInDynamo()
      .then(() => request(app)
        .get('/api/films')
        .expect('Content-Type', /json/)
        .expect(200))
      .then(response => {
        expect(response.body.length).toBe(1)
        expect(response.body[0].title).toBe('Save private Ryan')
      })
      .then(() => deleteInDynameFake(getTestFilm().id))
  })
  test('Given scan error in Dynamo When get all films Should return bad response', () => {
    const error = { error: 'Error connecting to DB' }
    const documentClient = {
      scan: (params, cb) => cb(error, null)
    }
    setDocumentClientAWS(documentClient)

    return request(app)
      .get('/api/films')
      .expect('Content-Type', /json/)
      .expect(400)
  })
})
