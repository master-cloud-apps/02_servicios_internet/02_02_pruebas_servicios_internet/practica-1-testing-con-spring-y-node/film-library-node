const app = require('../../../src/app.js')
const request = require('supertest')
const { createTestFilm } = require('../index.js')
const { deleteInDynameFake, setDocumentClientAWS } = require('./../../db/index.js')
const dynamoFake = require('../../db/dynamoFake.js')

jest.mock('aws-sdk')

describe('POST /api/films', () => {
  beforeAll(() => setDocumentClientAWS(dynamoFake))
  test('Given test Film When add the film Then return OK and added film ', () => {
    return createTestFilm(app)
      .expect(201)
      .then(response => {
        expect(response.body.id).toBeDefined()
        expect(response.body.title).toBe('Save private Ryan')
        return response.body.id
      })
      .then(id => deleteInDynameFake(id))
  })
  test('Given two created films When get all films Should return two elements', () => {
    return Promise.all([createTestFilm(app), createTestFilm(app)])
      .then(() => request(app).get('/api/films'))
      .then(response => {
        expect(response.body.length).toBe(2)
        return response.body.map(e => e.id)
      })
      .then(ids => Promise.all(ids.map(id => deleteInDynameFake(id))))
  })
  test('When put film with no title should return Bad Request', () => {
    return request(app).post('/api/films')
      .send({ title: undefined })
      .expect(400)
  })
})
