const request = require('supertest')
const { getTestFilm } = require('./../index.js')

const createTestFilm = (app) => {
  return request(app).post('/api/films').send(getTestFilm())
}

module.exports = { createTestFilm }
