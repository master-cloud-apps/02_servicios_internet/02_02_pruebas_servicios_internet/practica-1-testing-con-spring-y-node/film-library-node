const dynamoFake = require('./dynamoFake.js')
const { getTestFilm } = require('./../index.js')
const AWS = require('aws-sdk')

jest.mock('aws-sdk')

const TableName = 'films'
const createFilmInDynamo = () => dynamoFake.putAsync({ TableName, Item: getTestFilm() })
const deleteInDynameFake = (id) => dynamoFake.deleteAsync({ TableName, id })

const setDocumentClientAWS = mockedImplementation => {
  AWS.DynamoDB.DocumentClient.mockImplementation(() => mockedImplementation)
}

const setDynamoDBMockImplementation = (promise) => {
  AWS.DynamoDB.mockImplementation(() => {
    return {
      createTable: (params) => {
        return {
          promise
        }
      }
    }
  })
}

module.exports = {
  createFilmInDynamo, deleteInDynameFake, setDocumentClientAWS, TableName, setDynamoDBMockImplementation
}
