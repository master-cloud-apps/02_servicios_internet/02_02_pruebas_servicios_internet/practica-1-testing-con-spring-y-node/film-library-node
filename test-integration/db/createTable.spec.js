const createTable = require('../../src/db/createTable')
const AWS = require('aws-sdk')
const { GenericContainer } = require('testcontainers')

describe('createTable use cases', () => {
  let dynamoContainer
  beforeAll(async () => {
    dynamoContainer = await new GenericContainer('amazon/dynamodb-local', '1.13.6')
      .withExposedPorts(8000)
      .start()
    console.log(`Endpoint: http://${dynamoContainer.getHost()}:${dynamoContainer.getMappedPort(8000)}`)

    AWS.config.update({
      region: process.env.AWS_REGION || 'local',
      endpoint: process.env.AWS_DYNAMO_ENDPOINT || `http://${dynamoContainer.getHost()}:${dynamoContainer.getMappedPort(8000)}`,
      accessKeyId: 'xxxx',
      secretAccessKey: 'xxxxx'
    })
  }, 30000)

  afterAll(async () => {
    await dynamoContainer.stop()
  })
  test('When calling createTable with docker DynamoDB should return ok', () => {
    return createTable('sports').then(response => console.log(response))
  }, 20000)
})
