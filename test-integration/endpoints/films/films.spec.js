const request = require('supertest')
const { GenericContainer } = require('testcontainers')
const AWS = require('aws-sdk')
const app = require('./../../../src/app.js')
const createTable = require('./../../../src/db/createTable.js')
const { getTestFilm } = require('../../../test/index.js')

describe('GET /api/films', () => {
  let dynamoContainer

  beforeAll(async () => {
    dynamoContainer = await new GenericContainer('amazon/dynamodb-local', '1.13.6')
      .withExposedPorts(8000)
      .start()
    console.log(`Endpoint: http://${dynamoContainer.getHost()}:${dynamoContainer.getMappedPort(8000)}`)

    AWS.config.update({
      region: process.env.AWS_REGION || 'local',
      endpoint: process.env.AWS_DYNAMO_ENDPOINT || `http://${dynamoContainer.getHost()}:${dynamoContainer.getMappedPort(8000)}`,
      accessKeyId: 'xxxx',
      secretAccessKey: 'xxxxx'
    })
    await createTable('films')
  }, 30000)

  afterAll(async () => {
    await dynamoContainer.stop()
  })

  test('Given no films when get films should return 0 elements', () => {
    return request(app).get('/api/films')
      .expect(200)
  }, 20000)

  test('Given n films when adding a new one then gt all returns n+1 elements', async () => {
    let getResponse = await request(app).get('/api/films')
    const initialElements = getResponse.body.length

    const postResponse = await request(app).post('/api/films').send(getTestFilm())
      .expect(201)

    expect(postResponse.body.title).toBe('Save private Ryan')

    getResponse = await request(app).get('/api/films')
    expect(getResponse.body.length).toBe(initialElements + 1)
  }, 20000)
})
